# OneNav简约暗色主题

## 介绍

OneNav是一个PHP + Sqlite3开发的书签管理程序，支持设置登陆可见的分类和链接，非常适合自用。本主题删掉了杂七杂八的功能，只留下三个文件。

## 截屏
![image](https://nav.yuum.cn/nav.png)
### 演示地址：[https://nav.yuum.cn](https://nav.yuum.cn)

## 主题安装
把[Dark.zip](https://nav.yuum.cn/dark.zip)解压到`/templates/`。登录后台--主题设置开启使用。


### OneNav安装
#### 下载安装[OneNav](https://github.com/helloxz/onenav/releases)主程序。
#### 设置伪静态

<details>
<summary>Nginx伪静态:</summary>

```
#安全设置
location ~* ^/(class|controller|db|data|functions|templates)/.*.(db3|php|php5|sql)$ {
    return 403;
}
location ~* ^/(data)/.*.(html)$ {
        deny all;
}
location /db {
        deny all;
}

#伪静态
rewrite ^/click/(.*) /index.php?c=click&id=$1 break;
rewrite ^/api/(.*)?(.*) /index.php?c=api&method=$1&$2 break;
rewrite /login /index.php?c=login break;
```
</details>

<details>
<summary>Caddy伪静态:</summary>

```
nav.yuum.cn {
        root * /var/www/nav

        file_server

        php_fastcgi unix//run/php/php7.4-fpm.sock

respond  @foo "Access denied" 403 {
        close
}
@foo {
        path /class/* /controller/* /db/* /data/* /functions/* /templates/*
}
}
```
</details>

### 如果本主题对你有所帮助，欢迎请我一杯蜜雪冰城。

发送支付宝口令到 **i@yuum.cn** 就可以啦~~

有其它问题也可以反馈联系！

![赞赏码](https://nav.yuum.cn/zan.png)
