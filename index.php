<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8" />
	<title><?php echo $site['title']; ?> - <?php echo $site['subtitle']; ?></title>
	<meta name="keywords" content="<?php echo $site['keywords']; ?>" />
	<meta name="description" content="<?php echo $site['description']; ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://unpkg.com/mdui@1.0.2/dist/css/mdui.min.css" />
	<link rel="stylesheet" href="templates/<?php echo $template; ?>/static/style.css">
	<?php echo $site['custom_header']; ?>
</head>

<body class = "mdui-drawer-body-left mdui-appbar-with-toolbar mdui-loaded">
	<!--左侧抽屉导航-->
	<div class="mdui-drawer" id="drawer">
	
	  <ul class="mdui-list">
	  <li><h1># <?php echo $site['title']; ?></h1></li>
	  	<?php
			//遍历分类目录并显示
			foreach ($category_parent as $category) {
			//var_dump($category);
		?>
          <div class="mdui-collapse" mdui-collapse>
              <div class="mdui-collapse-item">
        <div class="mdui-collapse-item-header">
		<a href="#category-<?php echo $category['id']; ?>">
			<li class="mdui-list-item mdui-ripple">
				<div class="mdui-list-item-content category-name"><?php echo htmlspecialchars_decode($category['name']); ?></div>
			</li>
		</a>
        </div>
		</div>
        </div>
		<?php } ?>		
	  </ul>
	</div>
	<!--左侧抽屉导航END-->

	<!--正文内容部分-->
	<div class="mdui-container">
		<div class="mdui-row">
			<!-- 遍历分类目录 -->
            <?php foreach ( $categorys as $category ) {
                $fid = $category['id'];
                $links = get_links($fid);
            ?>
			<div id = "category-<?php echo $category['id']; ?>" class = "mdui-col-xs-12 mdui-typo-title cat-title">
				<?php echo htmlspecialchars_decode($category['name']); ?>
			</div>
			<!-- 遍历链接 -->
			<?php
				foreach ($links as $link) {
					//默认描述
					$id = $link['id'];
				//var_dump($link);
			?>
			<a href="<?php echo $link['url'] ?>" target="_blank" >
			<div class="mdui-col-lg-2 mdui-col-md-3 mdui-col-sm-4 mdui-col-xs-6 link-space" id = "id_<?php echo $link['id']; ?>"  link-url = "<?php echo $link['url']; ?>">

				<!--定义一个卡片-->
				<div class="mdui-card link-line mdui-hoverable">
							<div class="mdui-card-primary">
									<div class="mdui-card-primary-title link-title">
										<span class="link_title"><?php echo $link['title']; ?></span> 
									</div>
							</div>
				</div>
				<!--卡片END-->
			</div>
			</a>
			<?php } ?>
			<!-- 遍历链接END -->
			<?php } ?>
		</div>
		<!-- row end -->

		
	</div>
	<!--正文内容部分END-->
	<!-- footer部分 -->
	<footer>
		<?php if(empty( $site['custom_footer']) ){ ?>
		© 2023 Powered by <a target = "_blank" href="https://github.com/helloxz/onenav" rel = "nofollow">OneNav</a>.Theme by <a href="https://gitlab.com/mooe/dark" target="_blank">Dark</a>. <a href="/index.php?c=login" target="_blank">Admin</a>
		<?php }else{
			echo $site['custom_footer'];
		} ?>
	</footer>
	<!-- footerend -->
	
</body>

</html>
